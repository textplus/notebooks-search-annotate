def path_to_id(path):
    import os

    return os.path.basename(path)


def basename(path):
    import os

    return os.path.basename(path)


def find_in_text(search, text, search_options):
    import re

    regexsearch = search_options.get("regexsearch", True)
    contextbefore_size = search_options.get("contextbefore_size", 200)
    contextafter_size = search_options.get("contextafter_size", 200)
    casesensitive = search_options.get("casesensitive", False)
    results = []
    if regexsearch:
        ignorecase_flag = 0 if casesensitive else re.I
        pattern = re.compile(search, ignorecase_flag)
        for result in re.finditer(pattern, text):
            result_string = text[result.start() : result.end()]
            context_before_start = max(0, result.start() - contextbefore_size)
            context_after_end = min(len(text), result.end() + contextafter_size)
            context = (
                text[context_before_start : result.start()],
                text[result.end() : context_after_end],
            )
            results.append(
                (context[0], result_string, context[1], result.start(), result.end())
            )
    else:
        text_normalized = text if casesensitive else text.lower()
        search_normalized = search if casesensitive else search.lower()
        pos = 0
        while pos != -1:
            pos = text_normalized.find(search_normalized, pos)
            if pos == -1:
                break
            end = pos + len(search)
            context_before_start = max(0, pos - contextbefore_size)
            context_after_end = min(len(text), end + contextafter_size)
            match = text[pos:end]
            context = (text[context_before_start:pos], text[end:context_after_end])
            results.append((context[0], match, context[1], pos, end))
            pos += len(search)  # use pos += 1 to find overlapping matches
    return results


def find_in_files(search, paths, search_options):
    results_dict = {}
    if not search:
        return results_dict
    for path in paths:
        with open(path) as f:
            text = f.read()
        results = find_in_text(search, text, search_options)
        if results:
            results_dict[path] = results
    return results_dict


def get_docs_data(paths, path_annotations):
    import json
    from collections import defaultdict

    doc_ids_without_annotation = set([path_to_id(item) for item in paths])
    docs_by_annotation = defaultdict(list)
    with open(path_annotations) as f:
        annotations_data = json.load(f)
        for doc_id, annotations in annotations_data.items():
            for annotation in annotations:
                annotation["doc_id"] = doc_id
                anot_text = annotation["text"]
                docs_by_annotation[anot_text].append(annotation)
            doc_ids_without_annotation.remove(doc_id)
    for doc_id in doc_ids_without_annotation:
        annotations_data[doc_id] = []
    return annotations_data, dict(docs_by_annotation)


def group_dicts_by_property(items, property):
    from collections import defaultdict

    dicts_by_property = defaultdict(list)
    for item in items:
        val = item.get(property, "_UNSET_")
        dicts_by_property[val].append(item)
    return dict(dicts_by_property)


def action__overview_annotations(query_info={}, app_env={}):
    import json

    template = app_env["jinja_env"].get_template("overview.html")
    docs_data, docs_by_annotation = get_docs_data(
        app_env["paths"], app_env["path_to_annotations"]
    )
    html = template.render(
        view=query_info.get("view", "view1"),
        root_url=app_env["root_url"],
        notebookdir_rel=app_env["notebookdir_rel"],
        api_token=app_env["api_token"],
        annotations_dict=docs_by_annotation,
        docs_data=docs_data,
        docs_count=len(docs_data.keys()),
        request_details=json.dumps(query_info, indent=True, sort_keys=True),
    )
    return html


def action__annotations_table(query_info={}, app_env={}):
    import json
    import lib_annot

    template = app_env["jinja_env"].get_template("annot-table.html")
    docs_data, docs_by_annotation = get_docs_data(
        app_env["paths"], app_env["path_to_annotations"]
    )
    annotations_count = sum([len(annots) for doc_id, annots in docs_data.items()])
    html = template.render(
        view=query_info.get("view", "view1"),
        root_url=app_env["root_url"],
        notebookdir_rel=app_env["notebookdir_rel"],
        api_token=app_env["api_token"],
        annotations_dict=docs_by_annotation,
        annotations_count=annotations_count,
        docs_data=docs_data,
        fields=lib_annot.Annotation.sorted_keys(),
        request_details=json.dumps(query_info, indent=True, sort_keys=True),
    )
    return html


def action__annotations_summary(query_info={}, app_env={}):
    template = app_env["jinja_env"].get_template("annot-summary.html")
    property = query_info.get("property", "---")

    annotations_by_property = group_dicts_by_property(
        query_info.get("items", []), property
    )
    html = template.render(
        view=query_info.get("view", "view1"),
        root_url=app_env["root_url"],
        notebookdir_rel=app_env["notebookdir_rel"],
        api_token=app_env["api_token"],
        annotations_by_property=annotations_by_property,
        property=property,
    )
    return html


def action__show_doc(query_info={}, app_env={}):
    from xml.sax.saxutils import escape

    template = app_env["jinja_env"].get_template("singledoc.html")
    doc_text = ""
    # with open(temp_in) as f:
    #     query_info = json.load(f)
    view = query_info["view"]
    doc_id = query_info["docId"]
    highlight_range = query_info.get("highlightRange", ",,")
    hlrange_docid, hlrange_start, hlrange_end = highlight_range.split(",", 2)
    with open(app_env["paths_by_id"][doc_id]) as f:
        doc_text = f.read()
    docs_data, docs_by_annotation = get_docs_data(
        app_env["paths"], app_env["path_to_annotations"]
    )
    own_annotations = docs_data[doc_id]
    if hlrange_docid and hlrange_start and hlrange_end:
        own_annotations.append(
            {"docId": hlrange_docid, "start": hlrange_start, "end": hlrange_end}
        )
    positions_in_text = set([0, len(doc_text)])
    for i, annot in enumerate(own_annotations):
        own_annotations[i]["end"] = int(own_annotations[i]["end"])
        if own_annotations[i]["end"] < 0:
            own_annotations[i]["end"] = len(doc_text) + own_annotations[i]["end"]
        # end = min(end, len(doc_text) -1)
        own_annotations[i]["end"] = min(own_annotations[i]["end"], len(doc_text) - 1)
        own_annotations[i]["start"] = min(int(annot["start"]), len(doc_text) - 1)
        positions_in_text.add(own_annotations[i]["start"])
        positions_in_text.add(own_annotations[i]["end"])
    positions_in_text = sorted(list(positions_in_text))
    text_with_anchors = ""
    for i in range(len(doc_text)):
        if i in positions_in_text:
            text_with_anchors += f'<a class="pos pos-{i}"></a>'
        text_with_anchors += escape(doc_text[i])
    html = template.render(
        view=view,
        root_url=app_env["root_url"],
        notebookdir_rel=app_env["notebookdir_rel"],
        api_token=app_env["api_token"],
        doc_id=doc_id,
        doc_text=text_with_anchors,
        all_annotations=sorted(list(docs_by_annotation.keys())),
        own_annotations=own_annotations,
        highlight_range=highlight_range,
    )
    return html


def action__show_search_results(query_info={}, app_env={}):
    search = query_info["searchtext"]
    info_about_search = search
    template = app_env["jinja_env"].get_template("results.html")
    search_options = {
        "contextbefore_size": int(query_info["contextbefore_size"]),
        "contextafter_size": int(query_info["contextafter_size"]),
        "regexsearch": query_info["regexsearch"],
        "casesensitive": query_info["casesensitive"],
    }
    results_dict = find_in_files(search, app_env["paths"], search_options)
    results_count = sum([len(items) for items in results_dict.values()])
    html = template.render(
        view=query_info["view"],
        results_dict=results_dict,
        results_count=results_count,
        info_about_search=info_about_search,
        root_url=app_env["root_url"],
        notebookdir_rel=app_env["notebookdir_rel"],
        api_token=app_env["api_token"],
        content_category="search-results",
        app_env=app_env,
    )
    return html


def action__show_annotation_search_results(query_info={}, app_env={}):
    import re
    import lib_annot
    from collections import defaultdict

    docs_data, docs_by_annotation = get_docs_data(
        app_env["paths"], app_env["path_to_annotations"]
    )
    search = query_info.get("search", None)
    info_about_search = []
    annot_fields = lib_annot.Annotation.sorted_keys()
    contextbefore_size = int(query_info["contextbefore_size"])
    contextafter_size = int(query_info["contextafter_size"])
    casesensitive = query_info["casesensitive"]
    ignorecase_flag = 0 if casesensitive else re.I
    result_annots = []
    cache_texts = {}
    results_dict = defaultdict(list)

    if search:
        info_about_search = search
        pattern = re.compile(search, ignorecase_flag)
        for annot_text, annots in docs_by_annotation.items():
            if re.search(pattern, annot_text):
                result_annots += annots
        # annots = docs_by_annotation.get(search, [])
    else:  # search in each field specified in the query
        for k, v in query_info.items():
            if k in annot_fields:
                info_about_search.append(f"{k}={v}")
        info_about_search = ",".join(info_about_search)
        for doc_id, annots in docs_data.items():
            for annot in annots:
                for k, v in annot.items():
                    if k in query_info:
                        pattern = re.compile(query_info[k], ignorecase_flag)
                        if re.search(pattern, v):
                            result_annots.append(annot)
                            break

    for annot in result_annots:
        doc_path = app_env["paths_by_id"][annot["doc_id"]]
        if doc_path not in cache_texts:
            with open(doc_path) as f:
                text = f.read()
            cache_texts[doc_path] = text
        else:
            text = cache_texts[doc_path]
        start = int(annot["start"])
        end = int(annot["end"])
        if end < 0:
            end = len(text) + end
        context_start = max(0, start - contextbefore_size)
        context_end = min(len(text), end + contextafter_size)

        results_dict[doc_path].append(
            (
                text[context_start:start],
                text[start:end],
                text[end:context_end],
                start,
                end,
            )
        )
    results_count = sum([len(items) for items in results_dict.values()])
    template = app_env["jinja_env"].get_template("results.html")
    html = template.render(
        view=query_info["view"],
        results_dict=results_dict,
        results_count=results_count,
        info_about_search=info_about_search,
        root_url=app_env["root_url"],
        notebookdir_rel=app_env["notebookdir_rel"],
        api_token=app_env["api_token"],
        content_category="search-results",
    )
    return html


def action__update_annotations(query_info={}, app_env={}):
    import json

    annotations = query_info["annotations"]
    # with open("annotations_temp.json", "w") as f:
    #     f.write(json.dumps(annotations, indent=True, sort_keys=True))
    existing_annotations = {}
    with open(app_env["path_to_annotations"]) as f:
        existing_annotations = json.load(f)
    existing_annotations.update(annotations)
    with open(app_env["path_to_annotations"], "w") as f:
        f.write(json.dumps(existing_annotations, indent=True, sort_keys=True))
    return f'Saved annotations to {app_env["path_to_annotations"]}'


def action__set_default_attributes(query_info={}):
    import json

    attrs = query_info["attrs"]
    path_to_defaults = "cfg/attr_defaults.json"
    with open(path_to_defaults, "w") as f:
        f.write(json.dumps(attrs, indent=True, sort_keys=True))
    return f"Saved default attributes to {path_to_defaults}"


def action__edit_multiple(query_info={}, app_env={}):
    import json
    import os
    import lib_annot
    from datetime import datetime

    # return str(query_info)
    result_annots = []
    attrs = query_info["attrs"]
    delete_mode = True if attrs.get("text", None) == "" else False
    changed = "deleted" if delete_mode else "changed"
    signatures_to_change = set(query_info["annotationsInfo"])
    if not signatures_to_change:
        return f"No annotations {changed}"
    existing_annotations = {}
    with open(app_env["path_to_annotations"]) as f:
        existing_annotations = json.load(f)

    counter_changed = 0
    for docid, annots in existing_annotations.items():
        for annot in annots:
            sgn = lib_annot.Annotation(annot).get_annot_signature()
            if sgn in signatures_to_change:
                counter_changed += 1
                if not delete_mode:
                    for k, v in attrs.items():
                        annot[k] = v
                    result_annots.append(annot)
            else:
                result_annots.append(annot)

    result_annots = group_dicts_by_property(result_annots, "docId")
    backup_path = ".".join(
        (
            app_env["path_to_annotations"],
            datetime.now().strftime("%Y%m%d_%H%M%S"),
            "backup",
        )
    )
    os.rename(app_env["path_to_annotations"], backup_path)
    with open(app_env["path_to_annotations"], "w") as f:
        f.write(json.dumps(result_annots, indent=True, sort_keys=True))
    return f"{counter_changed} annotations {changed}."


def action__annotate_locations(query_info={}, app_env={}):
    import json

    existing_annotations = {}
    with open(app_env["path_to_annotations"]) as f:
        existing_annotations = json.load(f)
    all_changes = query_info["changes"]
    for doc_id, changes in all_changes.items():
        for chg in changes:
            chg_apply = True
            if chg["add_annotation"] or chg["remove_annotation"]:
                compare_key_add = (
                    f'{doc_id},{chg["start"]},{chg["end"]},{chg["add_annotation"]}'
                )
                compare_key_remove = (
                    f'{doc_id},{chg["start"]},{chg["end"]},{chg["remove_annotation"]}'
                )
                annots = existing_annotations[doc_id]
                annots_after_changes = []
                for annot in annots:
                    keep = True
                    compare_key = (
                        f'{doc_id},{annot["start"]},{annot["end"]},{annot["text"]}'
                    )
                    if compare_key == compare_key_remove:
                        keep = False
                    if compare_key == compare_key_add:
                        chg_apply = False
                    if keep:
                        annots_after_changes.append(annot)
                if chg_apply and chg["add_annotation"]:
                    annots_after_changes.append(
                        {
                            "doc_id": doc_id,
                            "start": chg["start"],
                            "end": chg["end"],
                            "text": chg["add_annotation"],
                        }
                    )
                existing_annotations[doc_id] = annots_after_changes
    with open(app_env["path_to_annotations"], "w") as f:
        f.write(json.dumps(existing_annotations, indent=True, sort_keys=True))
    return json.dumps(f'Updated file {app_env["path_to_annotations"]}')


def action__export_view_to_file(query_info={}, app_env={}):
    import os
    import shutil

    destdir = app_env["datadir"]
    destdir_web = f"{destdir}/web"
    if os.path.exists(destdir_web):
        shutil.rmtree(destdir_web)
    shutil.copytree("web", destdir_web)
    destfile = f'{destdir}/{query_info["tofile"]}'
    template = app_env["jinja_env"].get_template("export.html")
    html = template.render(exported_text=query_info["exported_text"])
    with open(destfile, "w") as f:
        f.write(html)
    return f"Created export file in {destdir}."


def action__select_data_locations(query_info={}, app_env={}):
    template = app_env["jinja_env"].get_template("fs.html")
    forms_data = [
        [
            {"type": "h4", "value": "Search and annotate plaintext files"},
            {
                "type": "input",
                "name": "corpus_dir",
                "label": "Corpus directory",
                "value": "projects/notebooks-search-annotate-data/userstories-text",
                "attrs": {"class": "action onclick", "data-action": "becomeTarget"},
            },
            {
                "type": "input",
                "name": "annotations_file",
                "label": "Annotations file (will be created if not there)",
                "value": "projects/notebooks-search-annotate-data/userstories-text-annotations.json",
                "attrs": {"class": "action onclick", "data-action": "becomeTarget"},
            },
            {
                "type": "input",
                "name": "datadir",
                "label": "Data directory (used for exports)",
                "value": "projects/notebooks-search-annotate-data",
                "attrs": {"class": "action onclick", "data-action": "becomeTarget"},
            },
            {
                "type": "button",
                "value": "Load data",
                "attrs": {
                    "class": "action onclick",
                    "data-action": "submitForm",
                    "data-action-server": "load_data",
                    "data-view": "view1",
                },
            },
        ],
        [],
    ]
    html = template.render(
        view=query_info.get("view", "view1"),
        root_url=app_env["root_url"],
        notebookdir_rel=app_env["notebookdir_rel"],
        api_token=app_env["api_token"],
        forms=forms_data,
    )
    return html


def action__load_data(query_info={}, app_env={}):
    import os
    import glob
    import lib_util

    datadir = f'{app_env["root_dir"]}/{query_info["datadir"]}'
    corpus_dir = f'{app_env["root_dir"]}/{query_info["corpus_dir"]}'
    path_to_annotations = f'{app_env["root_dir"]}/{query_info["annotations_file"]}'

    problems = ""
    if not os.path.exists(corpus_dir):
        problems = f'''Corpus directory {query_info["corpus_dir"]} not found. 
To download sample data, run the next cell, then run this cell again and ensure the paths are correct.'''
        # lib_util.ensure_dir_exists(corpus_dir)
    if not os.path.exists(path_to_annotations):
        lib_util.ensure_dir_exists(os.path.dirname(path_to_annotations))        
        with open(path_to_annotations, "w") as f:
            f.write("{}")

    paths = sorted(glob.glob(f"{corpus_dir}/*"))
    paths_by_id = {basename(path): path for path in paths}

    app_env["paths"] = paths
    app_env["path_to_annotations"] = path_to_annotations
    app_env["paths_by_id"] = paths_by_id
    app_env["datadir"] = datadir
    app_env["corpus_dir"] = corpus_dir

    if problems:
        return problems
    else:
        return action__overview_annotations(query_info, app_env)


def action__startview(query_info={}, app_env={}):
    return action__select_data_locations(query_info, app_env)
