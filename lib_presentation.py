def html_with_js_vars(js_vars={}):
    # from IPython.display import display, HTML
    from jinja2 import Environment, BaseLoader

    html_template = """<html>
<body>
    <script>
    {% for key, value in js_vars.items() %}
        {{ key }} = "{{ value }}";
        console.log("Variable {{ key }} set.");
    {% endfor %}
    </script>
    <div>Setup done.</div>
</body>
</html>"""
    template = Environment(loader=BaseLoader).from_string(html_template)
    html = template.render(js_vars=js_vars)
    return html


def jinja_env_filesystem(templates_dir):
    from jinja2 import Environment, FileSystemLoader, select_autoescape

    return Environment(
        loader=FileSystemLoader(templates_dir), autoescape=select_autoescape()
    )


def render_template_file(path, vars, **kwargs):
    import os

    env = kwargs.get("env", None)
    dirpath, filename = os.path.abspath(path).rsplit(os.sep, 1)
    jinja_env = env or jinja_env_filesystem(dirpath)
    template = jinja_env.get_template(filename)
    text = template.render(**vars)
    return text
