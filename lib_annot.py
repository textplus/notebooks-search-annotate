class Annotation:
    def __init__(self, data):
        from datetime import datetime

        self.docId = ""
        self.start = 0
        self.end = -1
        self.text = "---"
        self.timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.namespace = ""
        self.authors = ""
        self.comment = ""

        for k, v in data.items():
            setattr(self, k, v)

    def todict(self):
        return vars(self)

    def get_annot_signature(self):
        return f'{self.docId},{self.start},{self.end},{self.text}'

    @staticmethod
    def sorted_keys():
        return [
            "docId",
            "authors",
            "namespace",
            "timestamp",
            "text",
            "comment",
            "start",
            "end",
        ]
