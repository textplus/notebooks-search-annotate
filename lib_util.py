def read_value_from_json_file(srcfile, key, error_msg=""):
    import os
    import json
    import sys

    if not os.path.exists(srcfile):
        print(error_msg)
        return
    value = None
    with open(srcfile) as f:
        secrets = json.load(f)
        value = secrets.get(key, None)
    if value is None:
        if error_msg:
            sys.stderr.write(error_msg)
    return value


def read_api_token(secrets_file, error_msg_api_token):
    return read_value_from_json_file(secrets_file, "api_token", error_msg_api_token)


def tofile(dest, text, **kwargs):
    import sys

    verbose = kwargs.get("verbose", True)
    if verbose:
        print(f"Creating file {dest}")
    try:
        with open(dest, "w") as f:
            f.write(text)
            return dest
    except Exception as e:
        error_msg = f"Error writing file {dest}\n{repr(e)}"
        if verbose:
            sys.stderr.write(error_msg)
        return error_msg


def set_corpusdir(corpusdir):
    import os
    import json
    import sys

    corpusdir = os.path.realpath(corpusdir)
    try:
        if not os.path.exists("cfg"):
            os.makedirs("cfg")
        tofile(
            os.path.realpath("cfg/settings.json"), json.dumps({"corpusdir": corpusdir})
        )
    except Exception as e:
        sys.stderr.write(e)


def run_system_command(cmd_parts):
    # adapted solution from:
    # https://stackoverflow.com/questions/16198546/get-exit-code-and-stderr-from-subprocess-call
    import subprocess

    pipes = subprocess.Popen(cmd_parts, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    std_out, std_err = pipes.communicate()

    if pipes.returncode != 0:
        raise Exception(std_err.decode().strip())
    else:
        return std_out.decode(), std_err.decode()


def ensure_dir_exists(path):
    import os
    import logging

    logger = logging.getLogger(__name__)
    try:
        if not os.path.exists(path):
            os.makedirs(path)
        return path
    except Exception as e:
        logger.error(e)
        raise


def ensure_dir_empty(path):
    import os
    import shutil
    import logging

    logger = logging.getLogger(__name__)
    try:
        if os.path.exists(path):
            shutil.rmtree(path)
        os.makedirs(path)
        return path
    except Exception as e:
        logger.error(e)
        raise


def upload_to_clouddir(secretdata_file, clouddir_label, files_to_upload):
    import os
    import sys
    import json
    from urllib.parse import urlparse

    # read *secretdata_file* and extract the needed info

    problems = []
    if os.path.exists(secretdata_file):
        try:
            with open(secretdata_file) as f:
                secretdata = json.load(f)
                clouddir_link = secretdata.get(clouddir_label, None)
                if clouddir_link:
                    parsed = urlparse(clouddir_link)
                    scheme = parsed.scheme + "://" if parsed.scheme else ""
                    data_server = scheme + parsed.netloc
                    token_part_of_clouddir_link = parsed.path.split("/")[-1]
                else:
                    problems.append(
                        f"Key '{clouddir_label}' not found in {secretdata_file}"
                    )
        except Exception as e:
            problems.append(f"{repr(e)}")
    else:
        problems.append(f"File not found: {secretdata_file}")

    # do the upload if no problems occurred

    if problems:
        sys.stderr.write("\n".join(problems))
    else:
        print(f"Uploading to {data_server}")
        for file in files_to_upload:
            fname = os.path.basename(file)
            upload_result = run_system_command(
                [
                    "curl",
                    "-k",
                    "-T",
                    file,
                    "-u",
                    f"{token_part_of_clouddir_link}:",
                    f"{data_server}/public.php/webdav/{fname}",
                ]
            )
            print(upload_result[0])
            print(upload_result[1])


def download_from_url(url, dest, **kwargs):
    import os
    import sys
    import urllib.request

    force = kwargs.get("force", False)
    verbose = kwargs.get("verbose", True)
    if verbose:
        print(f"Downloading \n{url}\n -> {dest}\n")
    # when *force* is False, only proceed with the download
    # if path *dest* doesn't exist already
    if force or not os.path.exists(dest):
        try:
            urllib.request.urlretrieve(url, dest)
            return dest
        except Exception as e:
            error_msg = f"Download failed for {url}\n{repr(e)}"
            if verbose:
                sys.stderr.write(error_msg)
            return error_msg
    else:
        return dest


def split_textfile(srcpath, at_lines_containing, destdir):
    """Split the text within a file and write the resulting files to *destdir*.

    Example call: split_textfile("wahlverwandtschaften.txt", "es Kapitel", ".")
    """
    import os
    import re

    src = os.path.realpath(srcpath)
    destdir = os.path.realpath(destdir)
    part_counter = 1
    cur_part = ""
    with open(src) as f:
        lines = f.readlines() + [at_lines_containing]
        for line in lines:
            if re.search(at_lines_containing, line):
                with open(
                    f"{destdir}/part_{str(part_counter).zfill(6)}.txt", "w"
                ) as destf:
                    destf.write(cur_part)
                part_counter += 1
                cur_part = ""
            cur_part += line
