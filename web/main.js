window.mainFn = (viewName) => {
    let viewSelector = (viewName) ? `.${viewName} ` : "";
    let otherView = (viewName == "view1") ? "view2" : "view1";
    $(viewSelector + " *").off("click mouseover mouseenter mouseout mouseleave keydown keyup keypress change");
    let defaultsUrl = `${rootUrl}/api/contents/${notebookdir_rel}/cfg/attr_defaults.json`;

    let htmlBlocks = {
        label: (text, annotationInfo, attrs) => {
            return ` <span data-action-client="singleDoc_toggleSelectLabel" 
        class="label action badge badge-secondary" data-annotation-info="${annotationInfo}"
        data-authors="${attrs.authors}" data-namespace="${attrs.namespace}" data-comment="${attrs.comment}" data-timestamp="${attrs.timestamp}">${text}</span> `;
        }
    };
    let helpTexts = { "edit_annotation": "# Set the values below and click 'Annotate'. To delete\n# an annotation, set 'text' to the empty string: text=" };

    let sendQueryThenPushButton = (queryObj, btnLabel, tmpFileName = "tmp_req.json") => {
        writeToFile(`${rootUrl}/api/contents/${notebookdir_rel}/${tmpFileName}?token=${api_token}`,
            JSON.stringify(queryObj, null, 2),
            (resp) => {
                console.log(resp);
                btn$ = $(`button:contains(${btnLabel})`).eq(0);
                btn$.click();
            }
        );

    };

    // from: https://www.javascripttutorial.net/dom/css/check-if-an-element-is-visible-in-the-viewport/
    function isInViewport(element) {
        const rect = element.getBoundingClientRect();
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            rect.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    }
    // from: https://stackoverflow.com/questions/19448436/how-to-create-date-in-yyyymmddhhmmss-format-using-javascript 
    function pad2(n) { return n < 10 ? '0' + n : n }
    //

    let timestampFormatted = () => {
        var datetime = new Date();
        return `${datetime.getFullYear()}-${pad2(datetime.getMonth() + 1)}-${datetime.getDate()}`
            + ` ${pad2(datetime.getHours())}:${pad2(datetime.getMinutes())}:${pad2(datetime.getSeconds())}`;
    };


    let action_singleDoc_toggleSelectLabel = (evt, startNode, mode = null, classOn = "active badge-primary", classOff = "badge-secondary", classActive = "active") => {
        if (!evt && !startNode) {
            startNode = $(viewSelector + " .view-singledoc .label.active")[0];
        } else {
            startNode = startNode || evt.target;
        }
        let startNode$ = $(startNode);
        let mpText$ = $(getMPTextElem());
        if (!mode) {
            mode = startNode$.hasClass(classActive) ? "off" : "on";
        }
        if (mode === "off") {
            startNode$.removeClass(classOn).addClass(classOff);
            mpText$.val("");
            action_clearHighlighting(evt);
        } else {
            $(`${viewSelector} .${classActive}`).removeClass(classOn).addClass(classOff);
            startNode$.addClass(classOn).removeClass(classOff);
            let attrs = "";
            let attrsText = `text=${startNode$.text()}`;
            for (let prop of ["comment", "namespace", "authors", "timestamp"]) {
                let val = startNode$.attr("data-" + prop);
                attrs[prop] = val;
                attrsText += `\n${prop}=${val}`
            }
            setMPText(null, { "text": `${helpTexts["edit_annotation"]}\n${attrsText}\n#parse;` });
            action_clearHighlighting(evt);
            action_highlightRange(evt);
        }
    };

    let action_singleDoc_toggleSelectAnnotationClass = (evt) => {
        $(viewSelector + " .view-singledoc .label.active").each((i, elem) => {
            action_singleDoc_toggleSelectLabel(null, elem, "off");
        });
        let input_labelsToAdd$ = $(`${viewSelector} .app-menu`).find(".labels-to-add");
        input_labelsToAdd$.val($(evt.target).text());
    };

    let action_editAnnotation = async (evt) => {
        let activeLabel$ = $(viewSelector + " .label.active");
        let mpText$ = $(getMPTextElem());
        let mptext = mpText$.val().trim();
        let opts = parseMPText(mptext, "text");
        let text = opts["text"];
        let curSelection$ = $(".cur-selection");
        if (activeLabel$.length) { // edit mode if a label is selected 
            if (text) {
                activeLabel$.html(text);
                for (let prop of ["comment", "namespace", "authors", "timestamp"]) {
                    if (prop in opts) {
                        activeLabel$.attr("data-" + prop, opts[prop]);
                    }
                }
            } else {
                activeLabel$.remove();
                curSelection$.removeClass("cur-selection");
                action_clearHighlighting();
            }
            return;
        }
        if (!mptext) {
            setMPText(null, {
                "text": `${helpTexts["edit_annotation"]}\ntext=\ncomment=\n#parse;`
            });
            return;
        }

        if (curSelection$.length !== 2) {
            // if nothing is selected, select the whole text of the document
            let anchors$ = $("a.pos");
            anchors$.eq(0).addClass("cur-selection");
            anchors$.eq(-1).addClass("cur-selection");
            curSelection$ = $(".cur-selection");
        }
        let startOffset, endOffset;
        [startOffset, endOffset] = curSelection$.map((i, elem) => {
            return parseInt($(elem).attr("class").split("pos-")[1]);
        });
        [startOffset, endOffset].sort()

        let result$ = $(viewSelector + " .result");
        let resultId = result$.attr("data-result-id");
        let doclLabelsContainer$ = result$.closest(".view-singledoc").find(".doc-labels");
        let labelAlreadyThere = false;
        doclLabelsContainer$.find(".label").each((i, elem) => {
            if ($(elem).text() === text) {
                labelAlreadyThere = true;
                return false;
            }
        });
        if (true) { //if (!labelAlreadyThere)
            let defaults = JSON.parse((await readJsonFile(`${defaultsUrl}?${Date.now()}`)).content);
            opts["timestamp"] = timestampFormatted();
            let attrs = { ...defaults, ...opts };
            let newLabel$ = $(htmlBlocks["label"](text, `${resultId},${startOffset},${endOffset}`, attrs));
            newLabel$.appendTo(doclLabelsContainer$)
                .on("click", action_singleDoc_toggleSelectLabel)
            // .on("mouseover", action_highlightRange)
            // .on("mouseout", action_clearHighlighting);
            if (mpText$) {
                mpText$.val('');
            }
        }
        curSelection$.removeClass("cur-selection");
        action_clearHighlighting();
    };


    let action_saveAnnotations = (evt) => {
        let labels$ = $(viewSelector + " .doc-labels .label");
        let singledocView$ = $(viewSelector + " .view-singledoc");
        let annotationsData = {};
        if (singledocView$.length) {
            let doc_id = singledocView$.attr("data-docId");
            annotationsData[doc_id] = [];
        }
        (labels$).each((i, elem) => {
            let annotationText = $(elem).text().trim();
            let authors, namespace, comment;
            if (annotationText) {
                let annotationInfo = $(elem).closest(".resultbox").find(".result").attr("data-pos");
                if (singledocView$.length) {
                    annotationInfo = $(elem).attr("data-annotation-info");
                }
                let docId, start, end;
                [docId, start, end] = annotationInfo.split(",");
                if (!annotationsData[docId]) { annotationsData[docId] = [] };
                annotationsData[docId].push({
                    "text": annotationText,
                    "start": start,
                    "end": end,
                    "docId": docId,
                    "authors": $(elem).attr("data-authors"),
                    "namespace": $(elem).attr("data-namespace"),
                    "comment": $(elem).attr("data-comment"),
                    "timestamp": $(elem).attr("data-timestamp")
                });
            }
        });
        sendQueryThenPushButton({
            "action": "update_annotations",
            "annotations": annotationsData,
            "view": viewName
        }, "Info");

    };

    let action_annotationsOverview = () => {
        sendQueryThenPushButton({ action: "overview_annotations", "view": viewName }, viewName);
    };

    let action_annotationsTable = (evt, opts = {}) => {
        let queryOpts = { action: "annotations_table", "view": viewName }
        sendQueryThenPushButton({ ...queryOpts, ...opts }, viewName);
    };

    let gotoDoc = (evt, btnLabel, view) => {
        btnLabel = btnLabel || viewName;
        view = view || viewName;
        let docId = $(evt.target).attr("data-docId");
        let highlightRange = $(evt.target).closest(".result").attr("data-pos");
        if (!highlightRange) {
            let tr$ = $(evt.target).closest("tr");
            if (tr$.length) {
                let rowData = getRowData(tr$.get(0), getColNames(tr$.closest("table").get(0)));
                highlightRange = [rowData["docId"], rowData["start"], rowData["end"]].join(",")
            }
        }
        sendQueryThenPushButton({ docId: docId, action: "show_doc", "highlightRange": highlightRange, "view": view }, btnLabel);
    };

    let gotoDocOtherView = (evt) => {
        gotoDoc(evt, otherView, viewName);
    };

    let getSearchOptions = () => {
        let regexsearch = $(viewSelector + " .regexsearch").get(0).checked;
        let casesensitive = $(viewSelector + " .casesensitive").get(0).checked;
        let contextbefore_size = $(viewSelector + " .contextbefore_size").val();
        let contextafter_size = $(viewSelector + " .contextafter_size").val();
        return {
            "regexsearch": regexsearch,
            "casesensitive": casesensitive,
            "contextbefore_size": contextbefore_size,
            "contextafter_size": contextafter_size
        }
    };

    let action_search = () => {
        let searchText = $(viewSelector + " .searchtext").val();
        let searchOptions = getSearchOptions();
        let options = {
            "action": "show_search_results",
            "searchtext": searchText,
            "view": viewName
        }
        sendQueryThenPushButton({ ...searchOptions, ...options }, viewName);
    };

    let resultsHavingSelectedLabels = (operator) => {
        let resultsCount = 0;
        let labels$ = $(viewSelector + " .annotation-class.selected");
        let searchLabels = [];
        labels$.each(function (index) {
            searchLabels.push($(this).children(".annotation-class-text").text());
        });
        let allResults$ = $(viewSelector + " .result");
        allResults$.removeClass("selected");

        if (!searchLabels.length) {
            $(viewSelector + " .result.hidden").removeClass("hidden");
            resultsCount = allResults$.length;
        } else {
            allResults$.each(function (index, elem) {
                let hasAll = true;
                let hasEither = false;
                let select = false;
                if ((operator === "and") && searchLabels.length) {
                    select = true
                }
                let labels = [];
                $(elem).find(".annotation-label").each((index, elem) => {
                    labels.push($(elem).text());
                });
                for (let searchLabel of searchLabels) {
                    if (labels.includes(searchLabel)) {
                        hasEither = true;
                        if (operator === "or") {
                            select = true;
                            break;
                        }
                    } else {
                        hasAll = false;
                        if (operator === "and") {
                            select = false;
                            break;
                        }
                    }
                }
                if (select) {
                    resultsCount += 1;
                    $(elem).addClass("selected").removeClass("hidden");
                } else {
                    $(elem).removeClass("selected").addClass("hidden");
                }
            });
        }
        $(".docs_count").text(resultsCount);

    };

    let action_selectAnnotationClass = (evt) => {
        // evt.stopPropagation();
        let target$ = $(evt.target).closest(".annotation-class");
        if (target$.hasClass("selected")) {
            target$.removeClass("selected");
        } else {
            target$.addClass("selected");
        }
        let connect_operator = $(viewSelector + ' input[name="connect_operator"]:checked').val();
        resultsHavingSelectedLabels(connect_operator);
    };


    let action_highlightRange = (evt) => {
        let rangeInfo, targetNode, resultId, startOffset, endOffset, startNode$;
        startNode$ = $(viewSelector + " .view-singledoc");
        if (evt && evt.target && $(evt.target).is(".label")) {
            rangeInfo = $(evt.target).attr("data-annotation-info");
        } else {
            if (!startNode$.length) {
                return;
            }
            rangeInfo = startNode$.attr("data-highlight");
            if (!rangeInfo) { return; }
        }
        // targetNode = startNode$.find(".resultbox .result").get(0).firstChild;

        [resultId, startOffset, endOffset] = rangeInfo.split(",");
        startOffset = parseInt(startOffset);
        endOffset = parseInt(endOffset);
        let startAnchor = $(`a.pos-${startOffset}`).get(0);
        let endAnchor = $(`a.pos-${endOffset}`).get(0);
        highlightRangeBetweenAnchors(startAnchor, endAnchor);
    };

    let action_clearHighlighting__ = (evt) => {
        let sel = window.getSelection();
        if (sel.rangeCount > 0) {
            sel.removeAllRanges();
        }
    };

    let action_clearHighlighting = (evt) => {
        $(`${viewSelector} a.cur-selection`).remove();
        $(`${viewSelector} span.highlight`).each((i, elem) => {
            $(elem).replaceWith(elem.innerHTML);
        });
    };

    let highlightRangeBetweenAnchors = (startAnchor, endAnchor) => {
        // action_clearHighlighting();
        let range = document.createRange();
        let firstSpan = null;
        try {
            range.setStartBefore(startAnchor);
            range.setEndAfter(endAnchor);
            // surround text nodes in range with <span class="highlight"/> 
            let inRange = false;
            for (let node of $(range.commonAncestorContainer).contents().toArray()) {
                if (node === startAnchor) {
                    inRange = true;
                    continue;
                } else if (node === endAnchor) {
                    inRange = false;
                    break;
                } else {
                    if (inRange && node.nodeType == 3) {
                        let span = document.createElement("span");
                        $(span).addClass("highlight");
                        let hlrange = document.createRange();
                        hlrange.setStartBefore(node);
                        hlrange.setEndAfter(node);
                        hlrange.surroundContents(span);
                        if (!firstSpan) { firstSpan = span; }
                    }
                }
            };
        } catch (err) {
            console.log(err);
        };

        if (firstSpan) {
            let coords = firstSpan.getBoundingClientRect();
            let parentCoords = firstSpan.parentNode.getBoundingClientRect();
            if (!isInViewport(firstSpan)) {
                firstSpan.scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest" });
                // firstSpan.parentNode.scrollTo(coords.x, coords.y - parentCoords.y);
            }

        };
    };

    let action_highlightSelection = (evt, sel) => {
        if ($(viewSelector + " .view-singledoc .label.active").length) {
            action_singleDoc_toggleSelectLabel(null, null, "off");
        }
        action_clearHighlighting();
        let anchors = [];
        for (let point of [
            { node: sel.endNode, offset: sel.endOffset }, // work backwards
            { node: sel.startNode, offset: sel.startOffset }
        ]) {
            let range = document.createRange();
            range.setStart(point.node, point.offset);
            range.setEnd(point.node, point.offset);
            let anchor = document.createElement("a");
            range.surroundContents(anchor);
            let rangeFromStart = document.createRange();
            let firstNode = $(viewSelector + ".resultbox .result").get(0).firstChild;
            rangeFromStart.setStartBefore(firstNode);
            rangeFromStart.setEndBefore(anchor);
            let realOffset = rangeFromStart.toString().length; //prevAnchors$.length ? parseInt(prevAnchors$.eq(0).attr("class").split("pos-")[1]) + point.offset : point.offset;
            $(anchor).addClass("cur-selection pos pos-" + realOffset);
            anchors.unshift(anchor);
        }

        highlightRangeBetweenAnchors(anchors[0], anchors[1]);

    };

    let onchange_connectOperator = (evt) => {
        connect_operator = $(evt.target).val();
        resultsHavingSelectedLabels(connect_operator);
    };

    let getMPTextElem = () => {
        return $(viewSelector + " .multipurpose-text").get(0);
    };

    let parseMPText = (mptext, propNameWhenUnstructured = "mptext") => {
        mptext = mptext.trim();
        let opts = {};
        if (mptext) {
            if (mptext.endsWith("\n#parse;")) {
                for (let line of mptext.trim().split("\n")) {
                    line = line.trim();
                    if (line.indexOf("#") === 0) { continue; }
                    let parts = line.split("=");
                    let k = parts[0];
                    let v = parts.slice(1).join("=");
                    opts[k] = v;
                }
            } else {
                opts[propNameWhenUnstructured] = mptext;
            }

        }
        return opts;
    };

    let actionUsingMultipurposeTextBox = (evt, options, fn) => {
        let optionsAsText = "";
        let clickedLabel = $(evt.target).text();
        let actionName = $(evt.target).attr("data-action-server");
        let multipurposeText$ = $(".multipurpose-text");
        let mptext = multipurposeText$.val();
        if (mptext.includes(`action=${actionName}`)) {
            let opts = parseMPText(mptext);
            if (typeof fn === "function") {
                fn(opts);
            }
            sendQueryThenPushButton(opts, "Info");
        } else {
            for (let [key, value] of Object.entries(options)) {
                optionsAsText += `${key}=${value}\n`;
            }
            multipurposeText$.val(`# Check options below and click '${clickedLabel}' again.\n${optionsAsText}#parse;`);
        }
    };

    let action_exportView = (evt) => {
        actionUsingMultipurposeTextBox(evt, { "tofile": "export.html", "action": "export_view_to_file" }, (opts) => {
            opts.exported_text = $(viewSelector + " .main-app-content").get(0).innerHTML;
        })
    };
    let action_annotateAll = (evt) => {
        actionUsingMultipurposeTextBox(
            evt,
            { "add_annotation": "", "remove_annotation": "", "action": "annotate_locations" },
            (opts) => {
                let annotationsData = {};
                let locations = [];
                $(viewSelector + " .result").each((i, elem) => {
                    let docId, start, end;
                    [docId, start, end] = $(elem).attr("data-pos").split(",");
                    if (!annotationsData[docId]) { annotationsData[docId] = [] };
                    annotationsData[docId].push({ "add_annotation": opts["add_annotation"], "remove_annotation": opts["remove_annotation"], "start": start, "end": end, "docId": docId });
                    // locations.push($(elem).attr("data-pos"));
                });
                opts["changes"] = annotationsData;
                return opts;
            })
    };

    let action_searchAnnotations = (evt) => {
        let mptext = $(getMPTextElem()).val();
        let opts = parseMPText(mptext, "search");
        let targetView = opts.view || viewName;
        let searchOptions = getSearchOptions();
        let options = {
            "action": "show_annotation_search_results",
            "view": targetView
        }
        options = { ...searchOptions, ...opts, ...options };
        sendQueryThenPushButton(options, targetView);
    };

    let readJsonFile = async (url) => {
        let resp = await fetch(url);
        return resp.json();
    };

    let setAnnotAttributes = async (evt) => {
        let defaults = JSON.parse((await readJsonFile(`${defaultsUrl}?${Date.now()}`)).content);
        let opts = parseMPText($(getMPTextElem()).val());
        let trimmedOpts = {}
        for (let prop of ["authors", "namespace", "comment"]) {
            if (prop in opts) {
                trimmedOpts[prop] = opts[prop];
            }
        }
        if (
            !("authors" in trimmedOpts)
            || !("namespace" in trimmedOpts)
        ) {
            setMPText(null, {
                "text": [
                    "# Set values below and click on 'Cfg' again",
                    `authors=${defaults.authors}`,
                    `namespace=${defaults.namespace}`,
                    `comment=${defaults.comment}`,
                    "#parse;"
                ].join("\n")
            });
        } else {
            sendQueryThenPushButton({
                "action": "set_default_attributes",
                "attrs": trimmedOpts,
                "view": viewName
            }, "Info");
            $(getMPTextElem()).val("");
        }

    };

    let editMultiple = (evt) => {
        let mptext = $(getMPTextElem()).val();
        let opts = parseMPText(mptext);
        let trimmedOpts = {};

        if (opts["action"] !== "edit_multiple") {
            setMPText(null, {
                "text": [
                    "#Edit selected annotations.",
                    "#To delete annotations, leave the field text empty: text=",
                    "#text=",
                    "#comment=",
                    "#namespace=",
                    "#authors=",
                    "#timestamp=",
                    "action=edit_multiple",
                    "#parse;"
                ].join("\n")
            });
        } else {
            let proceed = false;
            for (let prop of ["text", "comment", "namespace", "authors", "timestamp"]) {
                if (prop in opts) {
                    trimmedOpts[prop] = opts[prop];
                    proceed = true;
                }
            }
            let deleteMode = false;
            if (trimmedOpts["text"] === "") {
                deleteMode = true;
            }
            let table$ = $(viewSelector + " table");
            let annotsInfo = [];
            let colnames = getColNames(table$.get(0));
            let rowsCount = 0;
            table$.find("tbody tr").each((i, elem) => {
                let select = $(elem).find(".select-row").prop("checked");// true; // to do: elaborate later
                let rowData = getRowData(elem, colnames);
                if (select) { // && deleteMode
                    $(elem).remove();
                    annotsInfo.push(`${rowData.docId},${rowData.start},${rowData.end},${rowData.text}`);
                } else {
                    rowsCount += 1;
                }
            });
            $(viewSelector + " .annotations_count").text("Reload table to see changes. " + rowsCount);

            sendQueryThenPushButton({
                "action": "edit_multiple",
                "annotationsInfo": annotsInfo,
                "attrs": trimmedOpts
            }, "Info");
            $(getMPTextElem()).val("");
            // action_annotationsTable(null, { "tmpFileName": "tmp_req2.json" });
        }

    };

    let setMPText = (evt, opts = {}) => {
        let text = ("text" in opts) ? opts["text"] : $(evt.target).text();
        $(viewSelector + " .multipurpose-text").val(text);
    };

    let setMPTextFromTable = (evt) => {
        let td$ = $(evt.target).closest("td");
        let colnum = td$.prevAll("td").length;
        let colname = td$.closest("table").find("thead th").eq(colnum).text();
        // let cssSel = "." + td$.closest("table").attr("class").split().join(".")
        text = [
            `${colname}=${$(evt.target).text()}`,
            `view=${otherView}`,
            // `selector=${viewSelector} ${cssSel}`,
            "#parse;"
        ]
        setMPText(evt, { "text": text.join("\n") });
    };

    // https://stackoverflow.com/questions/14267781/sorting-html-table-with-javascript/53880407#53880407
    let getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;

    let comparer = (idx, asc) => (a, b) => ((v1, v2) =>
        v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2)
    )(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));

    let sortTable = (evt) => {
        let th = evt.target.closest("th");
        let table = th.closest('table');
        let tbody = table.querySelector('tbody');
        let colnames = getColNames(table);
        let items = [];
        Array.from(tbody.querySelectorAll('tr')) //:nth-child(n+2)
            .sort(comparer(Array.from(th.parentNode.children).indexOf(th), this.asc = !this.asc))
            .forEach(tr => {
                tbody.appendChild(tr);
                items.push(getRowData(tr, colnames));
            });

        if (!$(table).hasClass("summary")) {
            sendQueryThenPushButton({
                "action": "annotations_summary",
                "items": items,
                "property": $(th).text()
            },
                otherView);

        }
    }

    let getRowData = (elem, colnames) => {
        let data = {}
        $(elem).find("td").each(function (i) {
            data[colnames[i]] = $(this).text();
        });
        return data;
    };

    let getColNames = (table) => {
        return $(table).find("thead th").map((i, elem) => { return $(elem).text() }).get();
    };

    let filterTable = (evt) => {
        let operator = "or";
        let opts = parseMPText($(getMPTextElem()).val());
        // if (!("selector" in opts)) { return; }
        let table$ = $(viewSelector + " .annot_table");
        let colnames = getColNames(table$.get(0));
        let searchFields = [];
        for (let opt in opts) {
            if (colnames.includes(opt)) {
                searchFields.push(opt);
            }
        }
        let rows$ = table$.find("tbody tr");
        let rowsCount = 0;
        rows$.each(function (i, elem) {
            let hasAll = true;
            let hasEither = false;
            let select = false;
            if ((operator === "and") && searchFields.length) {
                select = true
            }
            if (!searchFields.length) { select = true; }
            let rowData = getRowData(this, colnames);
            for (let searchField of searchFields) {
                if (rowData[searchField] === opts[searchField]) {
                    hasEither = true;
                    if (operator === "or") {
                        select = true;
                        break;
                    }
                } else {
                    hasAll = false;
                    if (operator === "and") {
                        select = false;
                        break;
                    }
                }
            }
            if (select) {
                rowsCount += 1;
                $(elem).removeClass("hidden");
            } else {
                // $(elem).addClass("hidden");
                $(elem).remove();
            }
        });
        $(viewSelector + " .annotations_count").text(rowsCount);
    };

    let toggleSelectAllRows = (evt) => {
        let checkbox$ = $(evt.target).closest("input");
        let allRows$ = $(viewSelector + ".select-row");
        if (checkbox$.prop("checked")) {
            console.log("Select all rows");
            allRows$.prop("checked", true);
        } else {
            console.log("DeSelect all rows");
            allRows$.prop("checked", false);
        }
    };

    let goToSettings = (evt) => {
        let serverAction = $(evt.target).closest(".action").attr("data-action-server"); 
        console.log("serverAction", serverAction, evt.target);
        sendQueryThenPushButton({"action": serverAction}, "view1");
    };

    let clientActions = {
        "singleDoc_toggleSelectLabel": action_singleDoc_toggleSelectLabel,
        "singleDoc_toggleSelectAnnotationClass": action_singleDoc_toggleSelectAnnotationClass,
        "editAnnotation": action_editAnnotation,
        // "editAnnotation": action_editAnnotation,
        "saveAnnotations": action_saveAnnotations,
        "annotationsOverview": action_annotationsOverview,
        "search": action_search,
        "selectAnnotationClass": action_selectAnnotationClass,
        "highlightRange": action_highlightRange,
        "clearHighlighting": action_clearHighlighting,
        "exportView": action_exportView,
        "annotateAll": action_annotateAll,
        "searchAnnotations": action_searchAnnotations,
        "gotoDoc": gotoDoc,
        "gotoDocOtherView": gotoDocOtherView,
        "annotationsTable": action_annotationsTable,
        "setMPTextFromTable": setMPTextFromTable,
        "filterTable": filterTable,
        "setAnnotAttributes": setAnnotAttributes,
        "editMultiple": editMultiple,
        "toggleSelectAllRows": toggleSelectAllRows,
        "sortTable": sortTable,
        "goToSettings": goToSettings
    };

    /* run at start: */
    if ($(viewSelector + " .view-singledoc").length) {
        action_highlightRange();
    }

    /* event handlers: */
    $(viewSelector + " .action").on("click", (evt) => {
        let actionElem$ = $(evt.target).closest(".action");
        $(".latest-action").removeClass("latest-action");
        actionElem$.addClass("latest-action");
        let clientAction = actionElem$.attr("data-action-client");
        if (clientAction && (clientAction in clientActions)) {
            if (!["editAnnotation", "singleDoc_toggleSelectLabel"].includes(clientAction)) {
                if ($(viewSelector + " .view-singledoc .label.active").length) {
                    action_singleDoc_toggleSelectLabel(null, null, "off");
                }
            }
            clientActions[clientAction](evt);
        }
    });
    // $(viewSelector + " .view-singledoc .label").on("mouseover", action_highlightRange);
    // $(viewSelector + " .view-singledoc .label").on("mouseout", action_clearHighlighting);
    $(viewSelector + ' input[name="connect_operator"]').on("change", onchange_connectOperator)
    $(viewSelector + " .view-singledoc").on("mouseup", (evt) => {
        let sel = window.getSelection();
        if (sel.type == "Range") {
            let [[selStart, startNode], [selEnd, endNode]] = [
                [sel.anchorOffset, sel.anchorNode],
                [sel.focusOffset, sel.focusNode]
            ].sort(
                function (a, b) {
                    return a[0] - b[0];
                }
            );
            sel = {
                start: selStart,
                end: selEnd,
                startNode: startNode, //sel.anchorNode,
                startOffset: selStart, //sel.anchorOffset,
                endNode: endNode, //sel.focusNode,
                endOffset: selEnd, //sel.focusOffset,
                type: "Range"
            };
            action_highlightSelection(evt, sel);
        }
    });

    // $(".action-loadAnnotations").on("click", action_loadAnnotations);
    // $(viewSelector + " .results .row.resultbox").on("click", (evt) => {
    //     let resultRow$ = $(evt.target).closest(".resultbox");
    //     $(".selected").removeClass("selected");
    //     resultRow$.addClass("selected");
    // };    

};
