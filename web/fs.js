window.fs = (viewName) => {
    let viewSelector = (viewName) ? `.${viewName} ` : "";
    let otherView = (viewName == "view1") ? "view2" : "view1";
    let rootUrl = window.rootUrl;
    let notebookdir_rel = window.notebookdir_rel;
    let api_token = window.api_token;
    $(viewSelector + " *").off("click mouseover mouseenter mouseout mouseleave keydown keyup keypress change");

    const sendQueryThenPushButton = (queryObj, btnLabel, tmpFileName = "tmp_req.json") => {
        writeToFile(`${rootUrl}/api/contents/${notebookdir_rel}/${tmpFileName}?token=${api_token}`,
            JSON.stringify(queryObj, null, 2),
            (resp) => {
                console.log(resp);
                btn$ = $(`button.jupyter-button:contains(${btnLabel})`).eq(0);
                btn$.click();
            }
        );

    };

    let clientActions = {};

    const activateHandlers = () => {
        $(viewSelector + " *").off("click mouseover mouseenter mouseout mouseleave keydown keyup keypress change");

        $(viewSelector + " .action.onclick").on("click", (evt) => {
            let actionElem$ = $(evt.target).closest(".action");
            let clientAction = actionElem$.attr("data-action");
            // console.log("clientAction",clientAction);
            clientActions[clientAction](evt);
        });

        $(viewSelector + " .action.ondblclick").on("dblclick", (evt) => {
            let actionElem$ = $(evt.target).closest(".action.ondblclick");
            let clientAction = actionElem$.attr("data-action-ondblclick");
            clientActions[clientAction](evt);
        });
    };

    const showContentsOf = async (url, targetSelector) => {
        // use URL param to avoid receiving a cached response
        const response = await fetch(`${url}?${Date.now()}` );
        const data = await response.json();
        let html = "";
        let items = data.content;
        items.sort((a, b) => a.name - b.name);
        items.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
        let relPath = url.replace(`${rootUrl}/api/contents/`, "");
        let relPathParts = relPath.split("/");
        let closeBtn = ``;
        let rootDirBtn = `<span class="action onclick fs-dir" data-action="showContents" data-path=""><img src="${rootUrl}/files/${notebookdir_rel}/web/img/folder.svg" alt="Root folder" width="16" height="16"/> </span>`;
        let breadCrumb = [closeBtn, rootDirBtn];
        for (let i = 0; i < relPathParts.length; ++i) {
            let part = relPathParts[i];
            let path = relPathParts.slice(0, i + 1).join("/");
            breadCrumb.push(`<span class="action onclick fs-dir" data-action="showContents" data-path="${path}">${part}</span>`);
        }
        let dirs = [`<div class="breadcrumb">${breadCrumb.join("/")}</div>`];
        let files = [];
        for (const item of items) {
            let attrs = Object.keys(item).map((k) => `data-${k}=${item[k]}`).join(" ");
            let itemAsHtml;
            if (item.type === "directory") {
                itemAsHtml = `<div class="fs-dir fs-item action onclick ondblclick" data-action="updateTarget" data-action-ondblclick="showContents" ${attrs}>${item.name}</div>`;
                dirs.push(itemAsHtml);
            } else {
                itemAsHtml = `<div class="fs-file fs-item action onclick" data-action="updateTarget" ${attrs}>${item.name}</div>`
                files.push(itemAsHtml);
            }
        }
        $(targetSelector).html(dirs.join("\n") + files.join("\n"));
        activateHandlers();
    };

    clientActions.showContents = (evt) => {
        let actionElem$ = $(evt.target).closest(".action");
        let url = `${rootUrl}/api/contents/${actionElem$.attr("data-path")}`;
        console.log(url);
        showContentsOf(url, viewSelector + " .fs-contents");
    };

    clientActions.becomeTarget = (evt) => {
        let actionElem$ = $(evt.target).closest(".action");
        $(viewSelector + " .fs-target").removeClass("fs-target");
        actionElem$.addClass("fs-target");
    };

    clientActions.updateTarget = (evt) => {
        let actionElem$ = $(evt.target).closest(".action");
        let path = actionElem$.attr("data-path");
        $(viewSelector + " .fs-target").val(path).removeClass("fs-target");
    };

    clientActions.toggleFileBrowser = (evt) => {
        let fsb$ = $(".fs-browser");
        if (fsb$.hasClass("hidden")){
            fsb$.removeClass("hidden");
        } else {
            fsb$.addClass("hidden");
        }
    };

    clientActions.submitForm = (evt) => {
        evt.preventDefault();
        let actionElem$ = $(evt.target).closest(".action");
        let targetView = actionElem$.attr("data-view");
        let form$ = actionElem$.closest("form");
        let formData = {"action": actionElem$.attr("data-action-server")};
        form$.find("input, textarea").each(function (i, elem) {
            let elem$ = $(elem);
            formData[elem$.attr("name")] = elem$.val();
        });
        console.log(formData);
        sendQueryThenPushButton(formData, targetView);
    };

    const startUrl = rootUrl + `/api/contents/${notebookdir_rel}`;
    showContentsOf(startUrl, viewSelector + " .fs-contents");


};