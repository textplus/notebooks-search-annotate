def form_entry(entry):
    html = ""
    tag = entry["type"] # input, textarea, checkbox, select, ${HTML tag}
    name = entry.get("name", "")
    label = entry.get("label", "")
    value = entry["value"]
    attrs = " ".join([ f'{k}="{v}"' for k, v in entry.get("attrs", {}).items() ])
    br = '<br />'
    if tag == "checkbox":       
        checked = 'checked="true"' if value else ""
        html = f'''<label for="{name}">{label}</label> <input type="checkbox" id="{name}" name="{name}" {checked} {attrs}/>'''
    elif tag == "select":
        options = [f'<option value="{item}">{item}</option>' for item in value]
        html = f'''<label for="{name}">{label}</label> <select id="{name}" name="{name}" {attrs}>{options}</select>'''
    elif tag == "input":
        html = f'''<label for="{name}">{label}</label><br /><input type="text" id="{name}" name="{name}" {attrs} value="{value}" />'''
    elif tag == "textarea":
        html = f'''<label for="{name}">{label}</label><br /><textarea id="{name}" name="{name}" {attrs}>{value}</textarea>'''
    else:
        html = f'<{tag} {attrs}>{value}</{tag}>'
        br = ""
    return html + br
