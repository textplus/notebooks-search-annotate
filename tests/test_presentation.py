import os

toplevel_dir = os.path.realpath(f"{__file__}/../..")
secrets_file = os.path.realpath(f"{toplevel_dir}/../.secrets")


def ensure_dir_in_syspath(dirpath):
    import sys

    if dirpath not in sys.path:
        sys.path.insert(0, dirpath)


def test_read_api_token():
    import lib_util

    api_token = lib_util.read_api_token(secrets_file, "problem")
    assert api_token


def test_html_with_js_vars():
    import lib_presentation

    html = lib_presentation.html_with_js_vars({"a": 1, "b": 2})
    assert 'a = "1";' in html


ensure_dir_in_syspath(toplevel_dir)


if __name__ == "__main__":
    test_read_api_token()
    test_html_with_js_vars()
