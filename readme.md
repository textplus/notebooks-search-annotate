# Search and annotate a corpus of plain text files

This is a web application running inside a Jupyter notebook (*00-main-app.ipynb*).
There are further notebooks for preparing the environment and for fetching and preparing appropriate data. 

The data used consists of the Text+ user stories. The data directory can be configured, meaning any folder containing text files can be used.

# How to use
* Login to jupyter-cloud.gwdg.de
* Get the repo from gitlab, by openning a terminal and running: 
  ```bash
  cd ~
  # clone with HTTPS:
  git clone https://gitlab.gwdg.de/textplus/notebooks-search-annotate.git
  ```
* In the Jupyterlab user interface, browse to the folder "notebooks-search-annotate".
* Run the notebook (*00-main-app.ipynb*)

*Further documentation will follow.*